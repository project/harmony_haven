# Harmony Haven

Harmony Haven is a fully responsive Drupal theme designed to provide a clean
and modern look to your website. It offers several customizable options,
manage header/ Footercopyright, and toggle the slider/banner display.
It also supports font awesome, and it is great for any kind of blog website.

## Table of contents

- Theme Features
- Requirements
- Installation
- Configuration
- Maintainers

## Theme Features

- Responsive, Mobile-Friendly Theme
- Mobile supported
- A total of 12 block regions
- Custom Slider
- Fully responsive layout, ensuring a seamless experience on various devices.
- Easy management of header,Footercopyright and slider through the Drupal
administration interface.
- Option to display a slider or banner on the homepage.

## Requirements

This theme requires no themes outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal theme. For further
information, see
[Installing Drupal Themes](https://www.drupal.org/docs/extending-drupal/installing-themes).

## Configuration

1. Navigate to Administration > Appearance and enable the theme.
2. Navigate to Administration > Appearance > Settings > Harmony Haven and
change the following:
  - Hide/show slider in front page.
  - Hide/show and change copyright text.
  - Hide/show and change Social Media Icon.
  - Add content for Navbar(Header), Footercopyright.

## Maintainers

- Soumya soni - [soumya soni](https://www.drupal.org/u/soumya-soni)
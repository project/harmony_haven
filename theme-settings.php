<?php

/**
 * @file
 * Implements().
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * @file
 * harmony_haven theme file.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function harmony_haven_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  if ($form['#attributes']['class'][0] == 'system-theme-settings') {
    $form['#attached']['library'][] = 'harmony_haven/theme.setting';

    $form['harmony_haven_info'] = [
      '#markup' => '<h2><br/>Advanced Theme Settings</h2><div class="messages messages--warning">Clear cache after making any changes in theme settings. <a href="../../config/development/performance">Click here to clear cache</a></div>',
    ];
  }
   // >>>>>>>>>>> Social Media Link
   $form['show_social_icon']['social_icon'] = [
    '#type' => 'details',
    '#title' => t('Social Media Link'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['show_social_icon']['social_icon']['show_social_icon'] = [
    '#type' => 'checkbox',
    '#title' => t('Show Social Icons'),
    '#default_value' => theme_get_setting('show_social_icon'),
    '#description'   => t("Show/Hide social media links"),
  ];
  $form['show_social_icon']['social_icon']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook Link'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['show_social_icon']['social_icon']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter Link'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['show_social_icon']['social_icon']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram Link'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];
     // >>>>>>>>>>Footer copyright>>>>>>>>>
     $form['footer_details'] = [
      '#type' => 'details',
      '#title' => t('Copyright'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    
    $form['footer_details']['show_copyright'] = [
      '#type' => 'checkbox',
      '#title' => t('Show Copyright'),
      '#default_value' => theme_get_setting('show_copyright'),
      '#description' => t("Show/Hide Copyright"),
    ];
    
    $form['footer_details']['footer_copyright'] = [
      '#type' => 'textarea',
      '#title' => t('Footer Copyright:'),
      '#default_value' => theme_get_setting('footer_copyright'),
      '#description' => t("Text area for Footer Copyright."),
    ];
  //>>>>>>>>>>>>>Left Header>>>>>>>>>>>>
  $form['left_header_details'] = [
    '#type' => 'details',
    '#title' => t('left header'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['left_header_details']['left_header_details'] = [
    '#type' => 'checkbox',
    '#title' => t('Show left header'),
    '#default_value' => theme_get_setting('left_header_details'),
    '#description'   => t("Show/Hide Copyright"),
  ];
  $form['left_header_details']['left_header_phone'] = [
    '#type' => 'textarea',
    '#title' => t('phone number'),
    '#default_value' => theme_get_setting('left_header_phone'),
  ];
  $form['left_header_details']['left_header_email'] = [
    '#type' => 'textarea',
    '#title' => t('email'),
    '#default_value' => theme_get_setting('left_header_email'),
  ];
  // Slide show
 
$form['busi_settings']['slideshow'] = array(
    '#type' => 'details',
    '#title' => t('Front Page Slideshow'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['busi_settings']['slideshow']['slideshow_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show slideshow'),
    '#default_value' => theme_get_setting('slideshow_display', 'harmony_haven'),
    '#description'   => t("Check this option to show Slideshow in front page. Uncheck to hide."),
  );
  $form['busi_settings']['slideshow']['slide'] = array(
    '#markup' => t('You can change the title, url and image of each slide in the following Slide Setting fieldsets.'),
  );
  $form['busi_settings']['slideshow']['slide_num'] = [
    '#type' => 'number',
    '#title' => t('Select Number of Slider Display'),
    '#min' => 1,
    '#required' => TRUE,
    '#default_value' => theme_get_setting('slide_num'),
    '#description' => t("Enter Number of slider you want to display"),
  ];
  $limit = theme_get_setting('slide_num');

  for ($i = 1; $i <= $limit; $i++) {
    $form['busi_settings']['slideshow']['slide' . $i] = array(
      '#type' => 'details',
      '#title' => t('Slide '.$i),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['busi_settings']['slideshow']['slide' . $i]['slide_title_' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Slide '.$i.' Title'),
      '#default_value' => theme_get_setting("slide_title_{$i}", "harmony_haven"),
    );
    $form['busi_settings']['slideshow']['slide' . $i]['slide_image_' . $i] = array(
      '#type' => 'managed_file',
      '#title' => t('Slide '.$i.' Image'),
      '#description' => t('Use same size for all the slideshow images(Recommented size : 1920 X 603).'),
      '#default_value' => theme_get_setting("slide_image_{$i}", "harmony_haven"),
      '#upload_location' => 'public://',
    );
    $form['busi_settings']['slideshow']['slide' . $i]['slide_url_' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Slide '.$i.' URL'),
      '#default_value' => theme_get_setting("slide_url_{$i}", "harmony_haven"),
    );
      $form['busi_settings']['slideshow']['slide' . $i]['slide_url_title_' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Slide '.$i.' title'),
      '#default_value' => theme_get_setting("slide_url_title_{$i}", "harmony_haven"),
    );
  }



}

